import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Max on 22.07.17.
 */


/*
Классы Candidate_GJJ,Candidate_self,HR организованы с использованием абстрактных классов Human и Candidate
Наследование: (Candidate_GJJ and Candidate_self)->Candidate->Human
               HR->Human
Инкапсуляция: Основные переменные класса Human private доступ к ним организован по средствам методов set and get.

Полиморфизм: методы interface Ianswer переопределяются в классах.

В задаче реализовано два интерфейса (стандартный-Ianswer и поумолчанию IDialog)

 */
public class TaskChOOP implements IDialog {

    public static void main(String[] args) {

        Candidate_GJJ c1 = new Candidate_GJJ("Max");
        Candidate_self c2 = new Candidate_self("Ivan");
        HR h1 = new HR("Boss");

        TaskChOOP inter = new TaskChOOP();
        inter.dialog_ask(h1,c1);


         Dialog dialog =new Dialog();


         dialog.dialog(h1,c2);
         dialog.dialog(c2,h1);



    }
}


abstract class Human implements Ianswer{  //Абстракция
private String name;
private  ArrayList<String> questions_list=new ArrayList<>();
private ArrayList<String> answers_list=new ArrayList<>();

public Human(){};
public String get_name() {return name;}

public void answer_list_add(int number,String text){
    this.answers_list.add(number,text);
}

public void answer_list_set(int number,String text){
    this.answers_list.set(number,text);

}
public String answers_list_get(int x){
    return answers_list.get(x);

}


    public void questions_list_add(int number,String text){
        this.questions_list.add(number,text);
    }

    public void questions_list_set(int number,String text){
        this.questions_list.set(number,text);

    }
    public String questions_list_get(int x){
        return questions_list.get(x);

    }
    public int questions_size(){
        return questions_list.size();
    }



public Human (String name){
    this.name=name;

   }


}


abstract class Candidates extends Human   //Наследование
{

    public Candidates(String name) {
        super(name);

        this.answer_list_add(0,"Hi! My name is "+this.get_name());

    }


}


class Candidate_GJJ extends Candidates {


    public Candidate_GJJ(String name) {

        super(name);
        this.answer_list_set(0,this.answers_list_get(0)+". I passed successfully getJavaJob exams and code reviews");
        this.answer_list_add(1,"Ok. I will waiting your call.");

    }


    @Override
    public void answer(int number) {

        try {
            System.out.println(this.get_name()+": "+this.answers_list_get(number));
        }
        catch (Exception e){
            System.out.println("I dont know :(");
        }

    }

    @Override
    public void question( int number) {
        try {
            System.out.println(this.get_name()+": "+this.questions_list_get(number));
        }
        catch (Exception e){
            System.out.println("I dont know :(");
        }

    }


}


class Candidate_self extends Candidates {

    public Candidate_self(String name) {
        super(name);
        this.answer_list_set(0,this.answers_list_get(0)+". I have been learning Java by myself, nobody examined how thorough is my knowledge and how good is my code.");
        this.answer_list_add(1,"Ok. Bye bye.");
        this.questions_list_add(0,"Can i go?");
    }

    @Override
    public void answer( int number) {
        try {
            System.out.println(this.get_name()+": "+this.answers_list_get(number));
        }
        catch (Exception e){
            System.out.println("I dont know :(");
        }
    }

    @Override
    public void question( int number) {
        try {
            System.out.println(this.get_name()+": "+this.questions_list_get(number));
        }
        catch (Exception e){
            System.out.println("I dont know :(");
        }
    }


}


class HR extends Human {


    public HR(String name) {
        super(name);
        this.questions_list_add(0,"Hi! Introduce yourself and describe your java expirience please.");

        this.questions_list_add(1,"Thanks! We call you later.");
this.answer_list_add(0,"Yes");

    }

    @Override
    public void answer( int number) {
        try {
            System.out.println(this.get_name()+": "+this.answers_list_get(number));
        }
        catch (Exception e){
            System.out.println("I dont know :(");
        }
    }

    @Override
    public void question(int number) {
        try {
            System.out.println(this.get_name()+": "+this.questions_list_get(number));
        }
        catch (Exception e){
            System.out.println("I dont know :(");
        }
    }



}

interface IDialog{

default void dialog_ask(Human h, Human c){

for (int x=0;x<h.questions_size();x++){

    System.out.println(h.get_name()+": "+h.questions_list_get(x));
    dialog_answ(c,x);

    if(x==h.questions_size()-1) System.out.println("------------");
}



}

default void dialog_answ(Human c,Integer number){

try {
    System.out.println(c.get_name()+": "+c.answers_list_get(number));
}
catch (Exception e){

    System.out.println("I dont know :(");
}

}

}

interface Ianswer{
  public void  answer(int number);
  public void question(int number);
}

class Dialog {



    public void dialog(Human h1, Human c1){
        for (int x=0;x<h1.questions_size();x++){
            h1.question(x);
            c1.answer(x);
            if(x==h1.questions_size()-1) System.out.println("------------");
        }

    }


}


