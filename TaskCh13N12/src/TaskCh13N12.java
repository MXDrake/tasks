import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Max on 21.07.17.
 */

public class TaskCh13N12 {
    public static ArrayList<Employeers> list=new ArrayList<>();

    public static void main(String[] args) {

        Employeers i = new Employeers();
        Employeers e1 = new Employeers("Max", "Anvarov", "Maratovich", "SPB", new GregorianCalendar(2014, 7, 1));
        Employeers e2 = new Employeers("Ivan", "Petrov", "Vasileevich", "SPB", new GregorianCalendar(2013, 10, 1));
        Employeers e3 = new Employeers("Jon", "Smith", "Smithtovich", "SPB", new GregorianCalendar(2016, 1, 1));
        Employeers e4 = new Employeers("Bill", "Clinton", "Obamovich", "SPB", new GregorianCalendar(2012, 1, 1));
        Employeers e5 = new Employeers("Oleg", "Rich", "Poor", "SPB", new GregorianCalendar(2011, 11, 1));
        Employeers e6 = new Employeers("Ura", "Jim", "Bean", "SPB", new GregorianCalendar(2015, 4, 1));
        Employeers e7 = new Employeers("Vitalei", "Severin", " ", "SPB", new GregorianCalendar(2016, 9, 1));
        Employeers e8 = new Employeers("Lena", "Lenishna", "Lenina", "SPB", new GregorianCalendar(2014, 4, 1));

        list.add(e1);
        list.add(e2);
        list.add(e3);
        list.add(e4);
        list.add(e5);
        list.add(e6);
        list.add(e7);
        list.add(e8);
        Calendar check_date = new GregorianCalendar();

        for (int x = 0; x < list.size(); x++) {

            i = list.get(x);


            if (check_date.get(Calendar.YEAR) - i.date_start.get(Calendar.YEAR) > 3) {
                System.out.println(i.first_name + " " + i.second_name + " " + i.middle_name + " " + i.address+" "+i.date_start.get(Calendar.YEAR)+"."+i.date_start.get(Calendar.MONTH));
            }
            if (check_date.get(Calendar.YEAR) - i.date_start.get(Calendar.YEAR) == 3 & check_date.get(Calendar.MONTH) - i.date_start.get(Calendar.MONTH) >= -1) {
                System.out.println(i.first_name + " " + i.second_name + " " + i.middle_name + " " + i.address +" "+i.date_start.get(Calendar.YEAR)+"."+i.date_start.get(Calendar.MONTH));
            }

        }




    }
}


class Employeers{

    String first_name;
    String second_name;
    String middle_name;
    String address;
    GregorianCalendar date_start;


    Employeers(String first_name,String second_name,String middle_name, String address,GregorianCalendar date_start){

        this.first_name=first_name;
        this.second_name=second_name;
        this.middle_name=middle_name;
        this.address=address;
        this.date_start=date_start;


    }

    Employeers(){

    }

}
