import java.time.chrono.IsoChronology;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Max on 24.07.17.
 */
public class TaskChOOPver2 {

    public static void main(String[] args) {

        Random_list r_list=new Random_list();
        ArrayList<Human> list_boss =r_list.getRandomHRList();
        ArrayList<Human> list_candidate= r_list.getRandomCandidateList();
        ISpeak dialog1,dialog2;

        for (int x=0;x< list_boss.size();x++){
            dialog1=list_boss.get(x);
            dialog2=list_candidate.get(x);
            dialog1.speak();
            dialog2.speak();
            System.out.println("------------------");
        }



    }
}

abstract class Human implements ISpeak{
    private String name;
    private ArrayList<String> questions=new ArrayList<>();

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Human(String name) {
        this.name = name;
    }

    public void addQuestions(String str) { this.questions.add(str);}

    public String getNumQuestions(int num) {return questions.get(num);}
}

interface ISpeak {
    void speak();
}

class Candidate_GJJ extends Human{


    public Candidate_GJJ(String name) {
        super(name);
        this.addQuestions ("Candidate-"+this.getName()+": "+"Hi! My name is "+this.getName()+" I passed successfully getJavaJob exams and code reviews.");
    }


    @Override
    public void speak() {
         System.out.println(this.getNumQuestions(0));
    }
}

class Candidate_self extends Human  {

    public Candidate_self(String name) {
        super(name);

        this.addQuestions("Candidate-"+this.getName()+": "+"Hi! My name is "+this.getName()+" I have been learning Java by myself, nobody examined how thorough is my knowledge and how good is my code.");
    }

    @Override
    public void speak() {
        System.out.println(this.getNumQuestions(0));

    }
}
class HR extends Human{


    public HR(String name) {
        super(name);
        this.addQuestions(this.getName()+": Hi! Introduce yourself and describe your java expirience please.");
    }

    @Override
    public void speak() {

 System.out.println(this.getNumQuestions(0));


    }
}
class Random_list{
  private   ArrayList<Human> candidate(){

        ArrayList<Human> list =new ArrayList<>();
        for (int x=0;x<10;x++) {
            int y = 0;
            String name_candidate = "";
            while (y < 3) {
                y++;
                char name_char = (char) ('A' + new Random().nextInt(24));
                name_candidate += name_char;
            }
            if (x%2==0)   list.add(new Candidate_self(name_candidate));
            else list.add(new Candidate_GJJ(name_candidate));
        }
        return list;
    }
   private ArrayList<Human> hr_list(){
        ArrayList<Human> list =new ArrayList<>();
        for (int x=0;x<10;x++) {
            int y = 0;
            String name_hr = "";
            while (y < 1) {
                y++;
                char name_char = (char) ( 'A'+ new Random().nextInt(24));
                name_hr = "Boss-"+name_char;
            }
          list.add(new HR(name_hr));

        }
        return list;
    }


    public ArrayList<Human> getRandomHRList(){
       return this.hr_list();
    }
    public ArrayList<Human> getRandomCandidateList(){
        return this.candidate();
    }
}





