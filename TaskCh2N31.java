import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Max on 18.07.17.
 */
public class TaskCh2N31 {
    public static void main(String[] args) throws IOException {
        BufferedReader read=new BufferedReader(new InputStreamReader(System.in));
        String str =read.readLine();
        char[] charArray=str.toCharArray();
        char x=charArray[1];
        charArray[1]=charArray[2];
        charArray[2]=x;
        System.out.println(charArray);
    }
}
